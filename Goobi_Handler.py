import json
import pandas as pd
import lxml.etree as ET
from datetime import datetime

# Based on documents TopDocStruct receive the ExL-Portfolio MaterialTypes (Code and Description) from the Mapping.json File for Portfolio Creation
def mapMODSDocStruct(DocStruct, data):
    df = pd.DataFrame.from_dict(data)
    searchStruct = [DocStruct]
    result_df = df.loc[df["MODSDocStruct"].isin(searchStruct)]
    resultSet = {}
    resultSet["code"] = (result_df["Code"].iloc[0])
    resultSet["desc"] = (result_df["Description"].iloc[0])
    return resultSet

def parseMetadata(filename):
    #mytree = ET.parse(f'metadata/{processId}/meta.xml',parser=ET.XMLParser(remove_blank_text=True))
    mytree = ET.parse(filename, parser=ET.XMLParser(remove_blank_text=True))
    mytree = mytree.getroot()
    return mytree

def findMetadata(mytree, attributeName="", xpathStr = ""):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/' }
    ET.register_namespace=namespaces
    if xpathStr == "":
        xpathStr = f".//goobi:metadata[@name='{attributeName}']/text()"
    #print(xpathStr)
    e = mytree.xpath(xpathStr, namespaces=namespaces)
    #print(e)
    return e