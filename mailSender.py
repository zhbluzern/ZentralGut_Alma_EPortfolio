import smtplib
from email.message import EmailMessage
import json

def send_email(subject, body, to_email):
    # read and parse MailConfigFile
    with open('mailConfig.json', 'r') as myfile:
        data=myfile.read()
    mailConfig = json.loads(data)
    msg = EmailMessage()
    msg.set_content(body)
    msg['Subject'] = subject
    msg['From'] = mailConfig["from"]
    msg['To'] = to_email
    msg['Reply-To'] = mailConfig["replyTo"]
    msg['Cc'] = mailConfig["Cc"]

    # Connect to local SMTP server
    server = smtplib.SMTP('localhost')

    # Send email
    server.send_message(msg)
    #server.send_message(msg, to_addrs=[to_email, to_cc])
    
    # Terminate the SMTP session and close the connection
    server.quit()

def setMailBody(mmsId, portfolioId, ark, title=""):
    mailBody = f"Für folgendes Katalogisat {title} (https://rzs.swisscovery.slsp.ch/permalink/41SLSP_RZS/1os1mmd/alma{mmsId}) wurde soeben ein Portfolio mit der ID {portfolioId} angelegt und mit folgendem Link gespeichert: https://n2t.net/{ark}"
    return mailBody

if __name__ == '__main__':
    # read and parse MailConfigFile
    with open('mailConfig.json', 'r') as myfile:
        data=myfile.read()
    mailConfig = json.loads(data)
    mmsId = "TEST_991171341103405501"
    ark =  "TEST:ark:/63274/zhb1sq6q"
    portfolioId = "123_Test"
    mailBody = setMailBody(mmsId,portfolioId,ark)
    send_email('[ZentralGut] Neues Portfolio angelegt - TEST', mailBody,mailConfig["To"])
