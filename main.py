import ExL_Portfolio 
import json
import Goobi_Handler as Goobi
import mailSender as mail
from dotenv import load_dotenv
import os
from datetime import datetime
import argparse
import src.goobiHandler.goobiJournal as goobiJournal
load_dotenv()

# Erstelle den ArgumentParser
parser = argparse.ArgumentParser(description='Mint an ARK for a Goobi Workflow Process', usage="python3 main.py -id {processid} -script /full/path/to/the/script/")
# Füge das Argument 'shoulder' hinzu
parser.add_argument('--vorgangId', '-id', required=True, help='Goobi-Vorgang-ID, dient zum Auslesen und Schreiben der Metadatan', )
parser.add_argument('--script_path', '-script', required=False, help='Wenn Aufruf über Goobi-Vorgang-Step erfolgt, hier vollen Pfad zum Verzeichnisses des Skripts übermitteln')
# Parse die Argumente
args = parser.parse_args()
# Zugriff auf das Argument 'vorgangId'
print(f"Empfangene Vorgang-ID: {args.vorgangId}")
if args.vorgangId:
    processId = args.vorgangId
else:
    processId = ""
#Zugriff auf das Argument 'script_path'
if args.script_path:
    os.chdir(args.script_path)
    scriptPath = args.script_path

ExL = ExL_Portfolio.ExL_Portfolio(os.getenv("almaApiKey"))
ExL_Bib = ExL_Portfolio.ExL_Bib(os.getenv("almaApiKey"))
collection = ExL.getCollection(params={"q":"keywords~ZentralGut"})
collectionId = collection['electronic_collection'][0]['id']
print(f"Collection-ID: {collectionId}")
services = ExL.getServices(collectionId)
serviceId = services['electronic_service'][0]['id']
print(f"Services-ID: {serviceId}")

# read and parse ConfigFile
with open('config.json', 'r') as myfile:
    data=myfile.read()
config = json.loads(data)
# read and parse MailConfigFile
with open('mailConfig.json', 'r') as myfile:
    data=myfile.read()
mailConfig = json.loads(data)
#print(config)
# metadataFolder-Path
metadataDir = config["metadataDir"]


#Einlesen des meta.xml-Files für den angesprochenen Vorgang
filename = f"{metadataDir}{processId}/meta.xml"
modsTree = Goobi.parseMetadata(filename)

#Read portfolio_ExampleEmpty.json
# read and parse ConfigFile
with open('portfolioBody_ExampleEmpty.json', 'r') as myfile:
    data=myfile.read()
portfolioData = json.loads(data)

#docType = Goobi.mapMODSDocStruct("Act", config["materialMapped"])
xpathStr = ".//mets:div[@DMDID='DMDLOG_0000']/@TYPE"
docType = Goobi.findMetadata(modsTree,xpathStr=xpathStr)
docTypeMapped = Goobi.mapMODSDocStruct(docType[0], config["materialMapped"])
print(docTypeMapped)
portfolioData["material_type"]["value"] = docTypeMapped["code"]
portfolioData["material_type"]["desc"] = docTypeMapped["desc"]

#ARK
ark = Goobi.findMetadata(modsTree,attributeName="ARK")
print(ark[0])
portfolioData["linking_details"]["url"] = f"https://n2t.net/{ark[0]}"
portfolioData["linking_details"]["static_url"] = f"https://n2t.net/{ark[0]}"

#CatalogIDDigital = MMSID
mmsId = Goobi.findMetadata(modsTree,attributeName="CatalogIDDigital")
print(mmsId[0])
#Handle if MMS-ID is IZ or NZ - for portfolio we need the IZ mmsID
if ExL_Bib.checkMMSIdisIZId(mmsId[0]) == True:  #print("IZ ID given")
    mmsId = mmsId[0]
elif ExL_Bib.checkMMSIdisIZId(mmsId[0]) == "Error" or ExL_Bib.checkMMSIdisIZId(mmsId[0]) == None: #print("get IZ ID by NZ ID")
    mmsId = ExL_Bib.checkMMSIdisNZId(mmsId[0])
print(f"IZ-MMS-ID: {mmsId}")
portfolioData["resource_metadata"]["mms_id"]["value"] = mmsId

#TitleDocMain = Title
title = Goobi.findMetadata(modsTree,attributeName="TitleDocMain")
print(title[0])
portfolioData["resource_metadata"]["title"] = title[0]

# ActivationDate Get the current date and time
now = datetime.utcnow()
# Format to string in "YYYY-MM-DDZ" format
portfolioData["activation_date"] = now.strftime('%Y-%m-%d') + 'Z'

# Create new Portfolio
print(json.dumps(portfolioData))
## 1. Check if portfolio is already created for this record
portfolios = ExL.getPortfolios(collectionId,serviceId)
existingPortfolio = ExL_Bib.getNumOfPortfolioByMMSId(mmsId)
## 2. If Check is false, create Portfolio
if existingPortfolio == 0:
    print(f"For MMS-ID {mmsId} exists no portfolio")
    newPortfolio = ExL.createPortfolio(collectionId,serviceId,portfolioData)
    #print(newPortfolio)
    print(f"New portfolio created with Id: {newPortfolio['id']}")
    # Send E-Mail that Portfolio was created
    mailBody = mail.setMailBody(mmsId,newPortfolio["id"],ark[0],title[0])
    mail.send_email('[ZentralGut] Neues Portfolio angelegt', mailBody,mailConfig["To"])
    goobiJournalMessage = f"Portfolio mit ID {newPortfolio['id']} für Katalogisat {mmsId} angelegt."
else:
    goobiJournalMessage = (f"For MMS-ID {mmsId} exists at least one portfolio.")

#Goobi-Workflow Journal-Entry
goobiJournal.writeJournal(processId, os.getenv("goobiApiToken"), journalMessage=goobiJournalMessage, journalType="info")